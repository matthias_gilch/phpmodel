<?php
/* *** *** *** *** *** *** *** *** *** *\
 * settings.php                        *
 * @author Matthias Gilch              *
 *                                     *
 * Here you can configure the PHPModel *
 * setup.                              *
\* *** *** *** *** *** *** *** *** *** */

// Database settings: Please read the documentation
$database = array(
    "default" => array(
        "engine" => "mysql",
        "host" => "localhost",
        "user" => "root",
        "password" => "password",
        "db" => "default",
    ),
);
