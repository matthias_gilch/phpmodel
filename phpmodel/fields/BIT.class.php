<?php
class field_BIT extends field{
    private $M = 1;

    public function __construct($M = 1 , $KEY = NULL , $NULL = true , $DEFAULT = NULL , $EXTRA = NULL)
    {
        $this->M = $M;
        $this->KEY_SET = $KEY;
        $this->NULL = $NULL;
        $this->DEFAULT = $DEFAULT;
        $this->EXTRA = $EXTRA;
    }

    public function describe_me()
    {
        $ret = "BIT(" . $this->M . ") ";

        if($this->KEY_SET == 'PRIMARY')
        {
            $ret .= "PRIMARY KEY ";
        }
        else if($this->KEY_SET == 'FOREIGN')
        {
            $ret .= "FOREIGN KEY "; //TODO syntax?
        }

        if($this->NULL == false)
        {
            $ret .= "NOT NULL ";
        }

        if($this->DEFAULT != NULL)
        {
            $ret .= "DEFAULT=" . $this->DEFAULT . " ";
        }

        if($this->EXTRA != NULL OR $this->EXTRA != '')
        {
            $ret .= $this->EXTRA . " ";
        }

        $ret .= ",";
        return $ret;
    }

    public function set($val)
    {
        if($val < 0 OR $val > (pow(2 , $M) - 1)) // Value in range?
        {
            echo "Value not in Range! \n";
            exit;
        }
        if($val == NULL AND $this->NULL == false)   // Value NULL?
        {
            echo "Value can't be NULL! \n";
            exit;
        }
        $this->value = $val;
    }
}
?>
