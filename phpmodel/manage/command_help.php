<?php
/* *** *** *** *** *** *** *** *** *** *\
 * command_help.php                    *
 *                                     *
 * @author Matthias Gilch              *
\* *** *** *** *** *** *** *** *** *** */

// Check if general help or help for a 
// specific command is required
function command_help($command)
{
    if(empty($command) OR $trim($command) == false)
    {
        // Print general help
        // TODO
        echo "\n";
        echo "Helptext for PHPModel manage.php \n";
        echo "Run 'php manage.php command' \n\n";
        echo "Commands:\n";
        echo "help [command]    - Shows help for 'command'\n";
        echo "\n";
        exit;
    }

    // Need help-text for a specific command
    switch($command)
    {
    default:
        command_help(false);
    }
    exit;
}

