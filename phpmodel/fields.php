<?php
/* *** *** *** *** *** *** *** *** *** *\
 * fields.php                          *
 *                                     *
 * @author Matthias Gilch              *
\* *** *** *** *** *** *** *** *** *** */

class field {
    private $KEY_SET = NULL;
    private $NULL = true;
    private $DEFAULT = NULL;
    private $EXTRA = "";

    private $value;

    public function set($val)
    {
        $this->value = $val;
    }

    public function get($val)
    {
        return $this->value;
    }
}

require_once(__DIR__ . "/fields/BIT.class.php");
?>
