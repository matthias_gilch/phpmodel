<?php
/* *** *** *** *** *** *** *** *** *** *\
 * manage.php                          *
 *                                     *
 * @author Matthias Gilch              *
\* *** *** *** *** *** *** *** *** *** */

// check if settings.php exists (contains user settings)
if(!file_exists(__DIR__ . "/phpmodel/settings.php"))
{
    throw_error(2);
}
else
{
    require_once(__DIR__ . "/phpmodel/settings.php");               //require settings.php
}

// check if manage_settings.php exists (contains setup
// data for manage.php
if(!file_exists(__DIR__ . "/phpmodel/manage/settings.php"))
{
    throw_error(3);
}
else
{
    require_once(__DIR__ . "/phpmodel/manage/settings.php");        //require manage/settings.php
}

// check if any required filed doesn't exist
foreach($REQUIRED_FILES as $req_file)
{
    if(!file_exists(__DIR__ . $req_file["path"]))
    {
        throw_error($req_file["errno"]);
    }
    else
    {
        require_once(__DIR__ . $req_file["path"]);
    }
}

// Running on command line?
if(strtolower(PHP_SAPI) !== 'cli')
{
    throw_error(1);
}

// Read the commands
$no_command = empty($argv[1]) OR trim($argv[1]) === '';
if($no_command)
{
    command_help(false);
}
switch($argv[1])
{
case 'help':
    command_help(false);
    break;
}

// Functions

// Function to throw a error
// @param errno{int} Number of error (described in errors.txt)
function throw_error($errno)
{
    // Define the Error-Codes
    $errors = array(
        0 => "Error in throw_error.",
        1 => "Please start manage.php from command line.",
        2 => "settings.php not found.",
        3 => "manage/settings.php not found.",
        4 => "manage/command_help.php not found.",
    );

    if(!isset($errors[$errno]))
    {
        throw_error(0);
    }

    echo "Error " . $errno . ": " . $errors[$errno] . "\n";
    exit;
}
?>
